from django.shortcuts import render

# Create your views here.

def landingpage(request):
    return render(request, 'story3_landing.html')

def about(request):
    return render(request, 'story3_about.html')

def profile(request):
    return render(request, 'story3_profile.html')

def gallery(request):
    return render(request, 'story3_gallery.html')

def portfolio(request):
    return render(request, 'story3_portofolio.html')
